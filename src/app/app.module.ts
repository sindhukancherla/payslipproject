import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { GetdataService } from './services/getdata.service';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import { NgForm } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {ReactiveFormsModule,FormsModule,FormControl,FormGroup,Validators,FormBuilder} from '@angular/forms';
// import { PasswordValidation } from './password-validation';
import { AppComponent } from './app.component';

import { LoginComponent } from './login/login.component';
import { EmployeeDetailsComponent } from './employee-details/employee-details.component';
import { PasswordresetComponent } from './passwordreset/passwordreset.component';
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';

export const appRoutes: Routes = [
    {path:'', 
    component: LoginComponent},
    {path:'employee-details', 
    component:EmployeeDetailsComponent},
    {path:'reset', 
    component:PasswordresetComponent}



    ];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    EmployeeDetailsComponent,
    PasswordresetComponent
    

  ],
  imports: [
    BrowserModule,
    FormsModule,
    BrowserAnimationsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes),
    ReactiveFormsModule,
    
  
    ],
    
  providers: [GetdataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
