import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute} from '@angular/router';
import { GetdataService } from '../services/getdata.service';
import { NgForm,NG_VALIDATORS } from '@angular/forms';
import 'rxjs/add/operator/filter';
import swal from 'sweetalert2';
import * as CryptoJS from 'crypto-js';
import {ReactiveFormsModule,FormsModule,FormControl,FormGroup,Validators,FormBuilder} from '@angular/forms';
// import { PasswordValidation } from './password-validation';
import {AbstractControl} from '@angular/forms';

@Component({
  selector: 'app-passwordreset',
  templateUrl: './passwordreset.component.html',
  styleUrls: ['./passwordreset.component.css']
})
export class PasswordresetComponent implements OnInit {

private token;
resetemployee:ResetEmployee;
selectedPassword='';
empreset={};
password='';
myGroup:FormGroup;
  constructor(private route: ActivatedRoute, private dataService:GetdataService,
  	private router: Router ,fb: FormBuilder) {
let pwdRegEx =/^(?=.*\d)(?=.*[A-Z])(?!.*[^a-zA-Z0-9@#$^+=])(.{8,15})$/ ;
    

    this.myGroup = fb.group({
      password: ['', [Validators.required,Validators.minLength(4), Validators.pattern(pwdRegEx)]],
       // [’’, [Validators.required, Validators.minLength(5)]],
      selectedPassword: ['', Validators.required]
    }, {
      validator: PasswordValidation.MatchPassword// your validation method
    })

     }

  ngOnInit() {
  // this.route
  //     .queryParams
  //     .subscribe(params => {
  //         this.accesstoken = params['#token'];
  //     });
  //     console.log(this.accesstoken);


  this.route.queryParams
      .filter(params => params.token)
      .subscribe(params => {
        console.log(params); // {token: "tokenvalue"}
        this.token=params.token;
      });
      if(this.token != undefined) {
      	console.log('inside token');
      	      this.dataService.validatePasswordLink(this.token).subscribe(
          (resetemployee) => {
            console.log("employee data in resetComponent"+ JSON.stringify(resetemployee));
            if(resetemployee.empNo != null) {
              localStorage.setItem('resetemployee',JSON.stringify(resetemployee));
          console.log()
          this.router.navigateByUrl('/reset');
        }else {
          swal({
  title: 'Invalid link or expired!',
  type: "error",
background:'lightblue'}).then(okay => {
   if (okay) {
    window.location.href = "http://192.168.60.176:4200/";
  }
});  

}
        }
          
          
        );
      }

  }

  onSubmitr()
  {

  	console.log('insidecomponent');
  	// this.resetemployee=JSON.parse(localStorage.getItem('resetemployee'));
  	console.log(this.resetemployee);

    swal({
     title: 'Please wait',
     background:'lightblue'
});
swal.showLoading();

    if(this.password == this.selectedPassword ) {
          this.empreset.password=this.selectedPassword;
           this.empreset.resetToken=this.token;
          var text = this.empreset.password;
     console.log(text);
//  var ciphertext = CryptoJS.AES.encrypt(text, 'secretkey123');
// console.log("Cipher text: " + ciphertext);
var iterations = 500;
    var keySize = 256;
    var salt = "$ecretkey";
    console.log("saltkey:"+salt);
  var output = CryptoJS.PBKDF2(text, salt, {
        keySize: keySize/32,
        iterations: iterations
    });

     this.empreset.password=output.toString(CryptoJS.enc.Base64);
    console.log(this.empreset);
    this.dataService.resetpwd(this.empreset).subscribe(
          (data) => {
swal.close();
console.log(data);
swal({ 
 title: "Password Reset Successfully",
  confirmButtonColor: '#3085d6',
 confirmButtonText: 'ok',
 background:'lightblue',
 type: "success"}).then(okay => {
   if (okay) {
    window.location.href = "http://192.168.60.176:4200/";
  }
});
      },
      error => {
        swal.close();
        console.log(error);
        if(error.status==0){
          swal({title:'Server is down,please try again later',background:'lightblue'})
        }else{

        console.log(JSON.stringify(error.json()));
      }
      })
      
    } else {
       swal({title:'passwords are not mached,please check again',background:'lightblue'});
    }
  }

}
export class PasswordValidation {

    static MatchPassword(AC: AbstractControl) {
       let password = AC.get('password').value; // to get value in input tag
       let selectedPassword = AC.get('selectedPassword').value; // to get value in input tag
        if(password != selectedPassword) {
            console.log('false');
            AC.get('selectedPassword').setErrors( {MatchPassword: true} )
        } else {
            console.log('true');
            return null
        }
    }
}
