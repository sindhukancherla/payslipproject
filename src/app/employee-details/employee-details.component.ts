import { Component, OnInit,ElementRef,ViewChild} from '@angular/core';
import { GetdataService } from '../services/getdata.service';
import { NgForm } from '@angular/forms';
import {Http, RequestOptions, Headers, Response} from '@angular/http';
import { Router, ActivatedRoute} from '@angular/router';
import {ReactiveFormsModule,FormsModule,FormControl,FormGroup,Validators,FormBuilder} from '@angular/forms';
import swal from 'sweetalert2'
import * as FileSaver from 'file-saver'; 
@Component({
  selector: 'app-employee-details',
  templateUrl: './employee-details.component.html',
  styleUrls: ['./employee-details.component.css']
})
export class EmployeeDetailsComponent implements OnInit {
	@ViewChild('selectedYear') selectedYear:ElementRef;
	@ViewChild('form16SelectedYear') form16SelectedYear:ElementRef;
selectedMonth='';
empData={};
employee:Employee;
isAdmin = false;
isUser=false;
enableEmpPage=false;
dateOfJoining:string[];
isSelectedCurrentYear= false;
years:number[] = [];
ActualMonths:any;
diffUser = true;
months =
[{id:1,month:'Jan'},{id:2,month:'Feb'},{id:3,month:'Mar'}
 ,{id:4,month:'Apr'}
 ,{id:5,month:'May'}
 ,{id:6,month:'June'}
 ,{id:7,month:'July'}
 ,{id:8,month:'Aug'}
 ,{id:9,month:'Sep'}
 ,{id:10,month:'Oct'}
 ,{id:11,month:'Nov'}
 ,{id:12,month:'Dec'}];
 selectedEmpdetails:string;
empnolist=[];
 names:string[]=[];
  // buttonDisabled: boolean;
  myGroup: FormGroup;
  myGroups: FormGroup;
  constructor(private route: ActivatedRoute,
        private router: Router,private dataService:GetdataService) { 
// this.buttonDisabled = false;
// console.log(this.buttonDisabled);
  }

  ngOnInit() {
    console.log("component");
  	this.employee=JSON.parse(localStorage.getItem('employee'));
    console.log(this.employee);
   		if(localStorage.getItem("employee") === null) {
  			alert("session timeout please login..")
  		 	this.router.navigateByUrl('');
  		} else 
      if (this.employee.role == "USER") {
        this.isUser=true;
        this.employee=JSON.parse(localStorage.getItem('employee'));
         this.dateOfJoining = this.employee.dateOfJoining.split('-');
         console.log(this.dateOfJoining);
         var dateOfJoiningYear =parseInt(this.dateOfJoining[0]);
         var dif = new Date().getFullYear() - dateOfJoiningYear;
         console.log(dif);
      for (var i = 0; i <=dif; i++) {
        //this.years.push(dateOfJoiningYear);
            this.years[i] = dateOfJoiningYear;
              dateOfJoiningYear++;
      } 
    }else {
      this.isAdmin=true;
        this.employee=(localStorage.getItem('employee'));
console.log(this.employee);
this.empnolist=JSON.parse(this.employee).empNoList;
console.log(this.empnolist);
var test1=Object.keys(this.empnolist);
console.log(test1);
var test11=Object.values(this.empnolist);
console.log(test11);

console.log(test1.length);
console.log(test11.length);

for(var i=0; i<test1.length; i++){
  var test= JSON.stringify(this.empnolist[test1[i]]);
  console.log(test);
   test = test.replace(/"/g,"");
  this.names.push(test1[i]+" "+test);
  console.log(test);
}
 }
this.myGroup = new FormGroup(
      {
        year : new FormControl('', Validators.required ),
        month : new FormControl('', Validators.required )
});

this.myGroups = new FormGroup(
      {
        year : new FormControl('', Validators.required ),
        // month : new FormControl('', Validators.required )
});
  			
			}
  		
  

    onClick(z) {
      
    	let indexz=+z
    	let date=new Date();
    	let currentYear=date.getFullYear();
    	let currentMonth=date.getMonth();
    	console.log(z);
    	let sYear=this.years[indexz];

    this.ActualMonths = [];
     if(z == 0) {
      var dateOfJoiningMonth =parseInt(this.dateOfJoining[1]);
        this.ActualMonths = [];
        var j=0;
        for(var a=dateOfJoiningMonth-1; a<12;a++) {
          this.ActualMonths[j]=this.months[a];
          j++;
        }
      } else if(sYear==currentYear) {
      	var i=0;
		for(var k=0; k<this.months[currentMonth].id-1;k++) {
          this.ActualMonths[i]=this.months[k];
          i++;
        }
        console.log(this.ActualMonths);
   }  else{

   		this.ActualMonths = this.months;
   } 
   // this.buttonDisabled = false;
   // console.log(this.buttonDisabled);
      }


   downloadPDF(){
    swal({
     title: 'Please Wait',
     background: 'lightblue'
});
swal.showLoading();
 		if(this.selectedMonth && !isNaN(this.selectedMonth) ){
 			

	   		this.empData.month=+this.selectedMonth;
		   	this.empData.year=this.years[this.selectedYear.nativeElement.value];
		    console.log(this.empData);

   			this.dataService.downloadPayslip(this.empData).subscribe(
          (response) => {
          	console.log(response);
             let body = response.json();
            console.log(body);
            if(body.size==0){
              swal.close();
              swal({title:"Payslip not generated,Please try later",background: 'lightblue'});
            }else{
              swal.close();
				let fileBlob = response.blob();
				let blob = new Blob([fileBlob], { 
				type: 'application/pdf' // must match the Accept type
				      });
				let filename = this.employee.empName +"/"+this.empData.month +"/"+this.empData.year+".pdf";
				FileSaver.saveAs(blob, filename);
				// console.log(response);
      }
    },
error => {
            swal.close();
        console.log(error);
        if(error.status==0){
        //JSON.stringify(error.json())
        swal({title:'Connection timeout due to server is down,please try again later',background: 'lightblue'});
      }else{
        console.log(JSON.stringify(error.json()));
      }
}
    );
      

 		}
   }

   downloadForm16(){
    swal({
     title: 'Please Wait',
     background: 'lightblue'
});
swal.showLoading();
   		this.empData.year=this.form16SelectedYear.nativeElement.value;
      console.log(this.empData.year);
      
this.dataService.downloadForm16(this.empData).subscribe(
          (response) => {
            let body = response.json();
            console.log(body);
            if(body.size==0){
              swal.close();
              swal({ title:"Form16 not generated,Please try later",background:'lightblue'});
            }else{
            swal.close();
				let fileBlob = response.blob();
				let blob = new Blob([fileBlob], { 
				type: 'application/pdf' 
				      });
				let filename =this.employee.empName + "-"+ this.empData.year +" Form16"+".pdf"
				FileSaver.saveAs(blob, filename);
          				          }
                          },

error => {
            swal.close();
        console.log(error);
        if(error.status==0){
        //JSON.stringify(error.json())
        swal({title:'Connection timeout due to server is down,please try again later',background:'lightblue'});
      }else{
        console.log(JSON.stringify(error.json()));
      }
    }

                          );
   
   
  

   }  


cambiar_login() {
console.log("....cambiar_login........");
document.querySelector('.cont_forms').className = "cont_forms cont_forms_active_login";  
document.querySelector('.cont_form_login').style.display = "block";
document.querySelector('.cont_form_sign_up').style.opacity = "0";               

setTimeout(function(){  document.querySelector('.cont_form_login').style.opacity = "1"; },400);  
  
setTimeout(function(){    
document.querySelector('.cont_form_sign_up').style.display = "none";
},200);  
  }

cambiar_sign_up(at) {

  console.log("....cambiar_sign_up........");
  document.querySelector('.cont_forms').className = "cont_forms cont_forms_active_sign_up";
  document.querySelector('.cont_form_sign_up').style.display = "block";
document.querySelector('.cont_form_login').style.opacity = "0";
  
setTimeout(function(){  document.querySelector('.cont_form_sign_up').style.opacity = "1";
},100);  

setTimeout(function(){   document.querySelector('.cont_form_login').style.display = "none";
},400);  

}    
ocultar_login_sign_up() {

document.querySelector('.cont_forms').className = "cont_forms";  
document.querySelector('.cont_form_sign_up').style.opacity = "0";               
document.querySelector('.cont_form_login').style.opacity = "0"; 

setTimeout(function(){
document.querySelector('.cont_form_sign_up').style.display = "none";
document.querySelector('.cont_form_login').style.display = "none";
},500);  
  
 }



testing(){
  swal({
     title: 'Please Wait',
     background: 'lightblue'
});
swal.showLoading();
  console.log(this.selectedEmpdetails);
  var empNo=this.selectedEmpdetails.substring(0, 3);
  console.log(empNo);
  this.enableEmpPage=true;
  

  var dropDown = document.getElementById("payslipyear");
        dropDown.selectedIndex = 0;
        this.ActualMonths = null;
  // var dropDown1 = document.getElementById("payslipmonth");
  //    dropDown1.selectedIndex = 0;
  // var length = dropDown1.options.length;
  // for (var i = length - 1 ; i >= 0 ; i--){
  //   dropDown1.remove(i);
  // }
     // for ( var i = 1; i <=length; i++) {
     //       dropDown1.options[i] = null;
     //       length = dropDown1.options.length;
     //        // console.log(dropDown1.options[i]);
     // }
  
  //     while(length--){
  //   dropDown1.remove(length);
  // }

  var dropDown2 = document.getElementById("formyear");
        dropDown2.selectedIndex = 0;

  // this.myGroup.select.selectedIndex="None";
  // var empNo=Object.keys(this.empnolist).find(key => this.empnolist[key] === this.selectedEmpdetails);
  // console.log(empNo);


// console.log(delete dateOfJoiningYear);
/*console.log(this.years.length);

for (var k=0 ;k<this.years.length;k++) { 
    delete this.years[k];

  }
  console.log(this.years);*/

this.dataService.getdetails(empNo).subscribe(
          (data) => {
            swal.close();
            this.data=JSON.parse(JSON.stringify(data));
            console.log(this.data);

// var dropDown2 = document.getElementById("formyear");
//         dropDown.selectedIndex = 0;
            //localStorage.setItem('employee',JSON.stringify(data));
              // console.log(data);
          // this.router.navigateByUrl('/employee-details');
          // this.Djoining=this.data.dateOfJoining;
          // console.log(this.Djoining);
          this.dateOfJoining = this.data.dateOfJoining.split('-');
         var dateOfJoiningYear =parseInt(this.dateOfJoining[0]);
         var dif = new Date().getFullYear() - dateOfJoiningYear;
         console.log(dif);
      for (var i = 0; i <=dif; i++) {
        //this.years.push(dateOfJoiningYear);
            this.years[i] = dateOfJoiningYear;
              dateOfJoiningYear++;
      }    
            this.employee=data;
          },
          error => {
            swal.close();
        console.log(error);
        if(error.status==0){
        //JSON.stringify(error.json())
        swal({ title:'Server is down,please try again later',
        background: 'lightblue'});
      }else{
        console.log(JSON.stringify(error.json()));
      }
      });
// document.getElementById("pform").reset();
// document.getElementById("fform").reset();

}


 onLogout(){
  swal({ title: "Are you sure",
 text: "You want to logout?",
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Yes',
  background: 'lightblue',
  showCancelButton: true,
 type: "warning"}).then((okay) => {
   if (okay.value) {
    window.location.href = "http://192.168.60.176:4200/";
  }
});
 }



}
interface Employee {
		empNo:number,
   		empName:string,
   		designation:string,
   		dateOfJoining:string,
   		panCard:string,
   		aadharCard:string,
      role:string,
      empNoList:any[];


}


