import { Injectable } from '@angular/core';
import { Http,Headers ,ResponseContentType,URLSearchParams} from '@angular/http';
import { HttpParams } from '@angular/common/http';
import 'rxjs/add/operator/map';
import {RequestOptions, Request, RequestMethod} from '@angular/http';
import * as FileSaver from 'file-saver'; 
@Injectable()
export class GetdataService {

  constructor(public http:Http) { 
  }
loginDetails(form) {
	return this.http
	.post("https://192.168.60.176:8443/login", form).map(res=>res.json());
}
downloadPayslip(form){
	let headers = new Headers({ 
   'Content-Type': 'application/json', 
   'Accept':'application/pdf'
});
let options=new RequestOptions({headers:headers});
options.responseType = ResponseContentType.Blob;
return this.http.post("https://192.168.60.32:8443/downloadpayslip", form,options);
}


downloadForm16(form){
	let headers = new Headers({ 
   'Content-Type': 'application/json', 
   'Accept':'application/pdf'
});
let options=new RequestOptions({headers:headers});
options.responseType = ResponseContentType.Blob;
return this.http.post("https://192.168.60.32:8443/downloadform", form,options);
}

forget(email) {
  
  console.log(email);

  
console.log('http://192.168.60.176:8443/forgot?email ='+email);
  return this.http.get('https://192.168.60.32:8443/forgot?email='+email).map((res: Response) => res.json());


  } 
validatePasswordLink(token) {
  console.log(token);
  return this.http.get('https://192.168.60.32:8443/reset?token='+token).map((res: Response) => res.json());
  } 

resetpwd(form) {
  console.log(form);
  return this.http.post("https://192.168.60.32:8443/reset",form);
  } 

  getdetails(empNo) {
  console.log(empNo);
  return this.http.get('https://192.168.60.32:8443/getDetails?EmpNo='+empNo).map((res: Response) => res.json());
  } 
}

