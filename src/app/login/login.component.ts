import { Component, OnInit, ElementRef,ViewChild } from '@angular/core';
import { GetdataService } from '../services/getdata.service';

import { NgForm } from '@angular/forms';
import {Http, RequestOptions, Headers, Response} from '@angular/http';
import { Router, ActivatedRoute} from '@angular/router';
import swal from 'sweetalert2';
import * as CryptoJS from 'crypto-js';
import {ReactiveFormsModule,FormsModule,FormControl,FormGroup,Validators,FormBuilder} from '@angular/forms';
import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';
import {BrowserModule} from '@angular/platform-browser';

import { Location } from '@angular/common';
// import { environment } from '../environments/environment';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  @ViewChild('emails') emails:ElementRef;
  public email='';
  employee:Employee;
  // emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";

myGroup: FormGroup;
myGroups: FormGroup;
location: Location;
  // username: FormControl;
  // Email: FormControl;
  // password: FormControl;
  // abcd:any;
  // efgh:any;

   constructor(private dataService:GetdataService,private route: ActivatedRoute,
        private router: Router) { 

    
}

  ngOnInit() {

    
    localStorage.clear();
     // this.createFormControls();
     // this.createForm();
let emailRegEx = /^[a-zA-Z]+@(?:(?:[a-zA-Z0-9-]+\.)?[a-zA-Z]+\.)?(osius)\.com$/;
// let userRegEx = /[a-zA-Z][a-zA-Z ]/ -- /^[a-zA-Z0-9_.+-]+@(?:(?:[a-zA-Z0-9-]+\.)?[a-zA-Z]+\.)?(osius)\.com$/;;
let userRegEx = /^[_A-z]*((-|\s)*[_A-z])*$/ ;
// let pwdRegEx = /^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{8,}$/ ;

this.myGroup = new FormGroup(
      {
        employeeId : new FormControl('', [Validators.required , Validators.minLength(4) , Validators.pattern(userRegEx)]),
        password : new FormControl('', [Validators.required , Validators.minLength(4)])
});

this.myGroups = new FormGroup(
      {
        email : new FormControl('', [Validators.required,Validators.email,
      Validators.pattern(emailRegEx)]),

    });


// if (environment.production) {
//    if (location.protocol === 'http:') {
//     window.location.href = location.href.replace('http', 'https');
//    }
//   }
  }
// createFormControls() {
//     this.abcd = new FormControl('', [Validators.required,Validators.minLength(8)]);
//     this.email = new FormControl('', [
//       Validators.required,
//       Validators.pattern("[^ @]*@[^ @]*")
//     ]);
//     this.efgh = new FormControl('', [
//       Validators.required,
//       Validators.minLength(8)
//     ]);
    
//   }

//   createForm() {
//     this.myGroup = new FormGroup({
//       name: new FormGroup({
//         abcd: this.abcd,
//       }),
//       email: this.email,
//       efgh: this.efgh,
//     });
//   }


  onSubmit() {
    console.log(this.myGroup.value);
    swal({
     title: 'Please Wait',
     background: 'lightblue'
});
swal.showLoading();
      console.log('inside component');
     //console.log(form.value);
var text = this.myGroup.value.password;
//  var ciphertext = CryptoJS.AES.encrypt(text, 'secretkey123');
// console.log("Cipher text: " + ciphertext);
//   form.value.password=ciphertext.toString();
//   console.log(form.value);

//   var decrypted = CryptoJS.AES.decrypt(ciphertext, "secretkey123");
// var plainText = decrypted.toString(CryptoJS.enc.Utf8)
// console.log("decrypted :" + plainText);  

    var iterations = 500;
    var keySize = 256;
    var salt = "$ecretkey";
   var output = CryptoJS.PBKDF2(text, salt, {
        keySize: keySize/32,
        iterations: iterations
    });

    this.myGroup.value.password=output.toString(CryptoJS.enc.Base64);
        this.dataService.loginDetails(this.myGroup.value).subscribe(
          (employee) => {
            swal.close();
            // console.log(employee);
            if(employee.role == "USER") {
              localStorage.setItem('employee',JSON.stringify(employee));
               console.log(employee);
          this.router.navigateByUrl('/employee-details');
        }else if(employee.role == "ADMIN"){
                  localStorage.setItem('employee',JSON.stringify(employee));
                  console.log(employee);
                  this.router.navigateByUrl('/employee-details');
        }else{
          swal.close();
          swal({
  title: 'Invalid credentials!',
  type: 'error',
  confirmButtonText: 'ok' ,
  background: 'lightblue', 
})
        }
          
          },
          error => {
            swal.close();
        console.log(error);
        if(error.status==0){
        //JSON.stringify(error.json())
        swal({ title:'Server is down,Please try again later',

        background: 'yellow'});
      }else if (error.status==500){
        console.log(JSON.stringify(error.json()));
        swal({title:'Internal server error,Please check console log for details',background:'yellow'});
      }else{
        console.log(JSON.stringify(error.json()));
      }
      }
        );
  }

onSubmitt() {
  // this.spinnerService.show();
  console.log('inside component');
  this.email=this.emails.nativeElement.value;
     // console.log(this.email);
   swal({
     title: 'Sending Mail',
    background: 'lightblue'
});
swal.showLoading();
     this.dataService.forget(this.email).subscribe(
          (data) => {
swal.close();
            console.log(data);
            if(data.Status== "true"){
             
            swal({title:'Password reset link sent to mail successfully',background: 'lightblue'});
         
          }
          else{
            swal({ title:'Email is not valid, please enter Official EmailAddress',background: 'lightblue'});

          }
          
      },
      error => {
        swal.close();
        if(error.status==0){
          swal({ 
            title:'server is down,please try again later',
            background: 'yellow'});
        }else{
        console.log(JSON.stringify(error.json()));
        swal({title:'please enter valid email address',background: 'lightblue'});
      }
      });
     
}


}

// function emailDomainValidator(control: FormControl) {
//   let email = control.value; 
//   if (email && email.indexOf("@") != -1) { 
//     let [_, domain] = email.split("@"); 
//     if (domain !== "osius.com") { 
//       return {
//         emailDomain: {
//           parsedDomain: domain
//         }
//       }
//     }
//   }
//   return null; 
// }

  interface Employee {
    empNo:number,
      empName:string,
      designation:string,
      dateOfJoining:string,
      panCard:string,
      aadharCard:string,
      role:string,
      empNoList:string[];

}

interface ResetEmployee {
    empNo:number,
      empId:string,
      empPassword:string,
      resetToken:string,
      email:string;

}


  